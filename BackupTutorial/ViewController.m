//
//  ViewController.m
//  BackupTutorial
//
//  Created by Saurabh on 30/11/18.
//  Copyright © 2018 Saurabh. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    DriveServiceManager *serviceManager;
}
@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong, nonatomic) IBOutlet UILabel *progressLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Configure Google Sign-in.
    GIDSignIn* signIn = [GIDSignIn sharedInstance];
    signIn.delegate = self;
    signIn.uiDelegate = self;
    signIn.scopes = [NSArray arrayWithObjects:kGTLRAuthScopeDrive, nil];
    [signIn signInSilently];
    
    // Add the sign-in button.
    self.signInButton = [[GIDSignInButton alloc] init];
    [self.signInButton setFrame:CGRectMake(30, 70, 150, 35)];
    [self.view addSubview:self.signInButton];
    
    [self.progressLabel setText:@"0.00 %"];
    
//    // Create a UITextView to display output.
//    self.output = [[UITextView alloc] initWithFrame:self.view.bounds];
//    self.output.editable = false;
//    self.output.contentInset = UIEdgeInsetsMake(20.0, 0.0, 20.0, 0.0);
//    self.output.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
//    self.output.hidden = true;
//    [self.view addSubview:self.output];
//
//    // Initialize the service object.
//    self.service = [[GTLRSheetsService alloc] init];
    
//    self.driveService = [[GTLRDriveService alloc] init];
//    MyManager *sharedManager = [MyManager sharedManager];
    serviceManager = [DriveServiceManager driveInstance];
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (user.userID.length > 0 ) {
        if (error != nil) {
            [self showAlert:@"Authentication Error" message:error.localizedDescription];
            //        self.service.authorizer = nil;
        } else {
            self.signInButton.hidden = true;
            //        self.driveService.authorizer = user.authentication.fetcherAuthorizer;
            serviceManager.driveService.authorizer = user.authentication.fetcherAuthorizer;
//            [self uploadFileToDrive];
            
            //        self.output.hidden = false;
            //        self.service.authorizer = user.authentication.fetcherAuthorizer;
            //        [self listMajors];
        }
    }
}

// Display (in the UITextView) the names and majors of students in a sample
// spreadsheet:
// https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
- (void)listMajors {
    self.output.text = @"Getting sheet data...";
    NSString *spreadsheetId = @"1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms";
    NSString *range = @"Class Data!A2:E";
    
    GTLRSheetsQuery_SpreadsheetsValuesGet *query =
    [GTLRSheetsQuery_SpreadsheetsValuesGet queryWithSpreadsheetId:spreadsheetId
                                                            range:range];
    [self.service executeQuery:query
                      delegate:self
             didFinishSelector:@selector(displayResultWithTicket:finishedWithObject:error:)];
}

// Process the response and display output
- (void)displayResultWithTicket:(GTLRServiceTicket *)ticket
             finishedWithObject:(GTLRSheets_ValueRange *)result
                          error:(NSError *)error {
    if (error == nil) {
        NSMutableString *output = [[NSMutableString alloc] init];
        NSArray *rows = result.values;
        if (rows.count > 0) {
            [output appendString:@"Name, Major:\n"];
            for (NSArray *row in rows) {
                // Print columns A and E, which correspond to indices 0 and 4.
                [output appendFormat:@"%@, %@\n", row[0], row[4]];
            }
        } else {
            [output appendString:@"No data found."];
        }
        self.output.text = output;
    } else {
        NSMutableString *message = [[NSMutableString alloc] init];
        [message appendFormat:@"Error getting sheet data: %@\n", error.localizedDescription];
        [self showAlert:@"Error" message:message];
    }
}

- (void) uploadFileToDrive {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        //    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"photo" ofType:@"jpg"];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //Create PDF_Documents directory
        documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"table.zip"];
        //    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
        NSData *fileData = [NSData dataWithContentsOfFile:documentsDirectory];
        
        GTLRDrive_File *metadata = [GTLRDrive_File object];
        //    metadata.name = @"photo.jpg";
        metadata.name = @"table.zip";
        
        //    GTLRUploadParameters *uploadParameters = [GTLRUploadParameters uploadParametersWithData:fileData
        //                                                                                   MIMEType:@"image/jpeg"];
        GTLRUploadParameters *uploadParameters = [GTLRUploadParameters uploadParametersWithData:fileData
                                                                                       MIMEType:@"application/zip"];
        uploadParameters.shouldUploadWithSingleRequest = TRUE;
        GTLRDriveQuery_FilesCreate *query = [GTLRDriveQuery_FilesCreate queryWithObject:metadata
                                                                       uploadParameters:uploadParameters];
        //    query.fields = @"id";
        
        query.executionParameters.uploadProgressBlock =
        ^(GTLRServiceTicket *ticket,
          unsigned long long numberOfBytesRead,
          unsigned long long dataLength) {
            NSLog(@"Ready %llu out of %llu", numberOfBytesRead, dataLength);
        };
        
        GTLRServiceTicket *uploadTicket = [self->serviceManager.driveService executeQuery:query completionHandler:^(GTLRServiceTicket *ticket,
                                                                                                              GTLRDrive_File *file,
                                                                                                              NSError *error) {
            if (error == nil) {
                NSLog(@"File ID %@", file.identifier);
            } else {
                NSLog(@"An error occurred: %@", error);
            }
        }];
        
        
        [uploadTicket.service setUploadProgressBlock:^(GTLRServiceTicket * _Nonnull progressTicket, unsigned long long totalBytesUploaded, unsigned long long totalBytesExpectedToUpload) {
            float myprogress = (100 *  ((float)totalBytesUploaded / (float)totalBytesExpectedToUpload));
            NSLog(@"PROGRESSS  ------  %2f", myprogress);
        }];
        
//        uploadTicket.service.uploadProgressBlock = ^(GTLRServiceTicket *ticket,
//                                                     unsigned long long numberOfBytesRead,
//                                                     unsigned long long dataLength) {
//            float myprogress = (1.0 / dataLength * numberOfBytesRead);
//            NSLog(@"PROGRESSS  ------  %f", myprogress);
//            int percent = (int)roundf(myprogress*100);
//            NSString *formattedNumber = [NSString stringWithFormat:@"%d", percent];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //                // do work here
//                self.progressLabel.text = [NSString stringWithFormat:@"%@ %@", formattedNumber, @"%"];
//                [self.progressView setProgress:myprogress animated:true];
//            });
//        };
    });
}

- (IBAction)getAllFiles:(id)sender {
    
    SecondViewController *secondView = [self.storyboard instantiateViewControllerWithIdentifier:@"SecondViewController"];
    [self.navigationController pushViewController:secondView animated:YES];
    
//    GTLRDriveQuery_FilesList *fileListquery;
//    fileListquery = [GTLRDriveQuery_FilesList query];
//    fileListquery.pageSize = 100;
//    //    fileListquery.q =[NSString stringWithFormat:@"'%@' in parents and trashed=false", @"root"];
//    fileListquery.q =[NSString stringWithFormat:@"mimeType='application/vnd.google-apps.folder' and '%@' in parents and trashed=false", @"root"];
//    [serviceManager.driveService executeQuery:fileListquery
//                            completionHandler:^(GTLRServiceTicket *ticket,
//                                                GTLRDrive_FileList *fileList,
//                                                NSError *error) {
//                                NSMutableArray *mainArray = [[NSMutableArray alloc]init];
//                                
//                                if (error != nil)
//                                {
//                                    NSLog(@"Error : %@", error);
//                                }
//                                else
//                                {
//                                    NSLog(@"Files Listing : %@", fileList.files);
//                                    if (fileList.files.count > 0) {
//                                        [mainArray addObjectsFromArray:fileList.files];
//                                    }
//                                }
//                                
//                            }];
}

- (IBAction)uploadAction:(id)sender {
    //Get path directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //Create PDF_Documents directory
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"table.zip"];
//    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"songzip"];
    [self uploadFileToDrive];
}

- (IBAction)downloadAction:(id)sender {
    
}

// Helper for showing an alert
- (void)showAlert:(NSString *)title message:(NSString *)message {
    UIAlertController *alert =
    [UIAlertController alertControllerWithTitle:title
                                        message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok =
    [UIAlertAction actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
     {
         [alert dismissViewControllerAnimated:YES completion:nil];
     }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
