//
//  SecondViewController.m
//  BackupTutorial
//
//  Created by Saurabh on 03/12/18.
//  Copyright © 2018 Saurabh. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController (){
    DriveServiceManager *serviceManager;
    NSMutableArray *folderListArray;
    UIRefreshControl *refreshControl;
}

@property (strong, nonatomic) IBOutlet UITableView *listingTableView;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.driveService = [[GTLRDriveService alloc] init];
    folderListArray = [[NSMutableArray alloc] init];
    serviceManager = [DriveServiceManager driveInstance];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        self.listingTableView.refreshControl = refreshControl;
    } else {
        [self.listingTableView addSubview:refreshControl];
    }
    
    UIBarButtonItem *refreshItem = [[UIBarButtonItem alloc] initWithTitle:@"Add"               style:UIBarButtonItemStylePlain target:self action:@selector(createAFolder)];
    self.navigationItem.rightBarButtonItem = refreshItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [self getAllFolders];
}

- (void)refreshTable {
    //TODO: refresh your data
    [self getAllFolders];
    [refreshControl endRefreshing];
    [self.listingTableView reloadData];
}

- (void)getAllFolders {
    [folderListArray removeAllObjects];
    GTLRDriveQuery_FilesList *fileListquery;
    fileListquery = [GTLRDriveQuery_FilesList query];
    fileListquery.pageSize = 100;
    fileListquery.q =[NSString stringWithFormat:@"'%@' in parents and trashed=false", @"root"];
//    fileListquery.q =[NSString stringWithFormat:@"mimeType='application/vnd.google-apps.folder' and '%@' in parents and trashed=false", @"root"];
    [serviceManager.driveService executeQuery:fileListquery
                  completionHandler:^(GTLRServiceTicket *ticket,
                                      GTLRDrive_FileList *fileList,
                                      NSError *error) {
                      if (error != nil) {
                          NSLog(@"Error : %@", error);
                      } else {
                          NSLog(@"Files Listing : %@", fileList.files);
                          if (fileList.files.count > 0) {
                              [self->folderListArray addObjectsFromArray:fileList.files];
                          }
                      }
                      dispatch_async(dispatch_get_main_queue(), ^{
                          [self.listingTableView reloadData];
                      });
                  }];
}

- (void)createFolder {
//    GTLRDrive_File *folderObj = [GTLRDrive_File object];
//    folderObj.name = @"TESTING Folder XYZ";
//    folderObj.mimeType = @"application/vnd.google-apps.folder";
//    
//    // To create a folder in a specific parent folder, specify the identifier
//    // of the parent:
//    // _resourceId is the identifier from the parent folder
//    
////    GTLDriveParentReference *parentRef = [GTLDriveParentReference object];
////    parentRef.identifier = parentId;
////    folderObj.parents = [NSArray arrayWithObject:parentRef];
////
////    if (driveFilesId.length && ![driveFilesId isEqualToString:@"0"]) {
////        GTLDriveParentReference *parentRef = [GTLDriveParentReference object];
////        parentRef.identifier = driveFilesId;
////        folderObj.parents = [NSArray arrayWithObject:parentRef];
////    }
//    
//    
//    GTLRQuery *query = [GTLRQuery queryForFilesInsertWithObject:folderObj uploadParameters:nil];
//    
//    GTLRServiceTicket *queryTicket =
//    [serviceManager.driveService executeQuery:query
//                                                  completionHandler:^(GTLRServiceTicket *ticket, id object,
//                                                                      NSError *error) {
//                                                      if (!error) {
////                                                          [[NSNotificationCenter defaultCenter] postNotificationName:@"DropboxCreateFolderSuccess" object:self userInfo:nil];
////                                                          [MBProgressHUD hideHUDForView:self.view animated:YES];
////                                                          [self viewWillAppear:YES];
////                                                          [tbDownload reloadData];
//                                                      }
//                                                      else
//                                                      {
//                                                          NSLog(@"Error %@",error);
//                                                      }
//                                                  }];
}

- (void)createAFolder {
//    GTLServiceDrive *service = self.driveService;
    
    GTLRDrive_File *folderObj = [GTLRDrive_File object];
    folderObj.name = [NSString stringWithFormat:@"Naruto %@", [NSDate date]];
    folderObj.mimeType = @"application/vnd.google-apps.folder";
    
    GTLRDriveQuery_FilesCreate *query = [GTLRDriveQuery_FilesCreate queryWithObject:folderObj
                                                                   uploadParameters:nil];
    
    [serviceManager.driveService executeQuery:query
                            completionHandler:^(GTLRServiceTicket *ticket, id object,
                                                NSError *error) {
                                if (!error) {
                                    NSLog(@"%ld", ticket.statusCode);
                                    [self getAllFolders];
                                } else {
                                    NSLog(@"Error %@",error);
                                }
                            }];
}

#pragma mark Tableview Delegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return folderListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    NSString *folderName = [[folderListArray objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", folderName];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *fileId = @"1p_1PXaO-Qx-A2woPb6akqQ1NsYjBjb7W";
    NSString *url = [NSString stringWithFormat:@"https://www.googleapis.com/drive/v3/files/%@?alt=media", fileId];
    GTMSessionFetcher *fetcher =
    [serviceManager.driveService.fetcherService fetcherWithURLString:url];
    
    [fetcher beginFetchWithCompletionHandler:^(NSData *data, NSError *error) {
        if (error == nil) {
            // Success.
            NSLog(@"%@", data);
            
            //Get path directory
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            
            //Create PDF_Documents directory
            documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"PDF_Documents"];
            [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:nil];
            
            NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"temp.pdf"];
            
            [data writeToFile:filePath atomically:YES];
//            completionBlock(data, nil);
        } else {
            NSLog(@"An error occurred: %@", error);
//            completionBlock(nil, error);
        }
    }];
    
    
//    GTLRDrive_File *folderObj = [GTLRDrive_File object];
//    folderObj.name = [NSString stringWithFormat:@"test.pdf"];
//    folderObj.mimeType = @"application/pdf";
    
//    [self downloadFileContentWithService:serviceManager.driveService
//                                    file:folderObj
//                         completionBlock:^(NSData *data, NSError *error) {
//                             if (data.length > 0) {
//                             }
//                         }];
    
    
//    [self searchInsideFolder:[[folderListArray objectAtIndex:indexPath.row] valueForKey:@"name"]];
//    NSString *fileId = @"1HLlQJoZyaBQzds2OUA9zqkj-NZvCZn6W";
//    GTLRQuery *query = [GTLRDriveQuery_FilesGet queryWithFileId:fileId];
//    [serviceManager.driveService executeQuery:query completionHandler:^(GTLRServiceTicket *ticket,
//                                                         GTLRDataObject *file,
//                                                         NSError *error) {
//        if (error == nil) {
//            NSLog(@"Downloaded %lu bytes", file.data.length);
//        } else {
//            NSLog(@"An error occurred: %@", error);
//        }
//    }];

    
//    GTMHTTPFetcher *fetcher =
//    [self.driveService.fetcherService fetcherWithURLString:downloadUrl];
//    //async call to download the file data
//    [fetcher beginFetchWithCompletionHandler:^(NSData *data, NSError *error) {
//        if (error == nil) {
//            NSLog(@"\nfile %@ downloaded successfully from google drive", self.selectedFileName);
//
//            //saving the downloaded data into temporary location
//            [data writeToFile:<path> atomically:YES];
//        } else {
//            NSLog(@"An error occurred: %@", error);
//        }
//    }];
}

- (void)searchInsideFolder: (NSString*)folderName {
    GTLRDriveQuery_FilesList *fileListquery;
    fileListquery = [GTLRDriveQuery_FilesList query];
    fileListquery.pageSize = 100;
//        fileListquery.q =[NSString stringWithFormat:@"'%@' in parents and trashed=false", folderName];
    fileListquery.q =[NSString stringWithFormat:@"mimeType='application/vnd.google-apps.folder' and '%@' in parents and trashed=false", folderName];
    [serviceManager.driveService executeQuery:fileListquery
                            completionHandler:^(GTLRServiceTicket *ticket,
                                                GTLRDrive_FileList *fileList,
                                                NSError *error) {
                                if (error != nil) {
                                    NSLog(@"Error : %@", error);
                                } else {
                                    NSLog(@"Files Listing : %@", fileList.files);
                                }
                            }];
}

- (void)downloadFileContentWithService:(GTLRDriveService *)service
                                  file:(GTLRDrive_File *)file
                       completionBlock:(void (^)(NSData *, NSError *))completionBlock {
    
    
    //  NSURLRequest *downloadRequest = [service requestForQuery:query];
    //  GTMSessionFetcher *fetcher = [service.fetcherService fetcherWithRequest:downloadRequest];
    //
    //  [fetcher setCommentWithFormat:@"Downloading %@", file.name];
    //  fetcher.destinationFileURL = destinationURL;
    //
    //  [fetcher beginFetchWithCompletionHandler:^(NSData *data, NSError *error) {
    //    if (error == nil) {
    //      NSLog(@"Download succeeded.");
    //
    //      // With a destinationFileURL property set, the fetcher's callback
    //      // data parameter here will be nil.
    //    }
    //  }];
    
    if (file.userProperties != nil) {
        // More information about GTMHTTPFetcher can be found on
        // <a href="http://code.google.com/p/gtm-http-fetcher">http://code.google.com/p/gtm-http-fetcher</a>
        NSString *url = [NSString stringWithFormat:@"https://www.googleapis.com/drive/v3/files/%@", file.name];
        GTMSessionFetcher *fetcher =
        [service.fetcherService fetcherWithURLString:@""];
        
        [fetcher beginFetchWithCompletionHandler:^(NSData *data, NSError *error) {
            if (error == nil) {
                // Success.
                completionBlock(data, nil);
            } else {
                NSLog(@"An error occurred: %@", error);
                completionBlock(nil, error);
            }
        }];
    } else {
        completionBlock(nil,
                        [NSError errorWithDomain:NSURLErrorDomain
                                            code:NSURLErrorBadURL
                                        userInfo:nil]);
    }
}


@end
