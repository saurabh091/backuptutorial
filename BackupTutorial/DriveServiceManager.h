//
//  DriveService.h
//  BackupTutorial
//
//  Created by Saurabh on 03/12/18.
//  Copyright © 2018 Saurabh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GTLRDrive.h>
#import <GTLRDriveQuery.h>

NS_ASSUME_NONNULL_BEGIN

@interface DriveServiceManager : NSObject{
    GTLRDriveService *driveService;
}

@property (nonatomic, strong) GTLRDriveService *driveService;

+ (id)driveInstance;

@end

NS_ASSUME_NONNULL_END
