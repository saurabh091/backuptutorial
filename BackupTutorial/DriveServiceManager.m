//
//  DriveService.m
//  BackupTutorial
//
//  Created by Saurabh on 03/12/18.
//  Copyright © 2018 Saurabh. All rights reserved.
//

#import "DriveServiceManager.h"

@implementation DriveServiceManager

@synthesize driveService;

+ (id)driveInstance {
    static DriveServiceManager *driveInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        driveInstance = [[self alloc] init];
    });
    return driveInstance;
}

- (id)init {
    if (self = [super init]) {
        driveService = [[GTLRDriveService alloc] init];;
    }
    return self;

}

@end
