//
//  ViewController.h
//  BackupTutorial
//
//  Created by Saurabh on 30/11/18.
//  Copyright © 2018 Saurabh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import <GTLRSheets.h>
#import <GTLRDrive.h>
#import <GTLRDriveQuery.h>
#import "DriveServiceManager.h"
#import "SecondViewController.h"

@interface ViewController : UIViewController <GIDSignInDelegate, GIDSignInUIDelegate>

@property (nonatomic, strong) IBOutlet GIDSignInButton *signInButton;
@property (nonatomic, strong) UITextView *output;
@property (nonatomic, strong) GTLRSheetsService *service;
@property (nonatomic, strong) GTLRDriveService *driveService;



@end

