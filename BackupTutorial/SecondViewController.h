//
//  SecondViewController.h
//  BackupTutorial
//
//  Created by Saurabh on 03/12/18.
//  Copyright © 2018 Saurabh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GTLRDrive.h>
#import <GTLRDriveQuery.h>
#import "DriveServiceManager.h"
#import <GTMOAuth2/GTMOAuth2ViewControllerTouch.h>
#import <GTMOAuth2/GTMOAuth2Authentication.h>
#import <GTMOAuth2/GTMOAuth2SignIn.h>
#import <Google/SignIn.h>

NS_ASSUME_NONNULL_BEGIN

@interface SecondViewController : UIViewController

@property (nonatomic, strong) GTLRDriveService *driveService;

@end

NS_ASSUME_NONNULL_END
